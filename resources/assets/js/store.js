export class Field {
  constructor(props) {
    this.readonly = false
    Object.assign(this, props)
    if (!this.label) {
      this.label = this.name
    }
  }
}

export class Row {
  constructor(props) {
    this.visible = true
    Object.assign(this, props)
  }

  match(name, string) {
    let v = this[name]
    if ('string' === typeof v) {
      v = v.toString()
    }
    this.visible = v.indexOf(string) >= 0
  }
}

export default class Store {
  constructor(props) {
    this.data = []
    this.params = {}
    this.columns = []
    this.busy = false
    Object.assign(this, props)
    if (!this.remote) {
      this.remote = axios.create({
        baseURL: '/' + this.name,
      })
    }
  }

  async load(params) {
    if (params) {
      params = Object.assign({}, this.params, params)
    }
    else {
      params = this.params
    }
    this.busy = true
    const {data: {meta, result}} = await this.remote.get('', {params})
    let attributes = meta ? Object.values(meta.attributes) : []
    if (result.length > 0) {
      for (const name in result[0]) {
        let column = this.columns.find(c => name === c.name)
        if (!column) {
          this.columns.push(new Field(attributes.find(c => name === c.name) || {name}))
        }
      }
      this.params._meta = 0
    }
    this.data = result.map(r => new Row(r))
    this.params = params
    this.busy = false
  }

  getRow(id) {
    return this.data.find(r => id === r.id)
  }

  getRows(ids) {
    return ids.map(id => this.getRow(id))
  }

  getCell(id, name) {
    return this.data.find(r => id === r.id)[name]
  }

  setCell(id, name, value) {
    this.getRow(id)[name] = value
  }

  async saveCell(id, name, value) {
    if (this.getCell(id, name) !== value) {
      if (await this.remote.post('?id=' + id, {[name]: value})) {
        this.setCell(id, name, value)
      }
    }
  }

  derive() {
    return new Store(this)
  }

  sort(order) {
    const strings = []
    for (let name in order) {
      const o = order[name]
      if (-1 === o) {
        name = '-' + name
      }
      if (o !== 0) {
        strings.push(name)
      }
    }
    this.load({_sort: strings.join(',')})
  }
}
