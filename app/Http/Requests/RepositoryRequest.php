<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;

class RepositoryRequest extends Request
{
    public function sort(): array
    {
        $sort = [];
        $order = $this->get('_sort');
        if ($order) {
            foreach (explode(',', $order) as $o) {
                $i = SORT_ASC;
                if ('-' === $o[0]) {
                    $i = SORT_DESC;
                    $o = substr($o, 1);

                }
                $sort[$o] = $i;
            }
        }
        return $sort;
    }
}
