<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index(Request $request): array
    {
        $v = $this->validate($request, [
            'type' => 'required',
            '_sort' => 'max:90',
            '_meta' => 'boolean',
            '_refresh' => 'boolean',
        ]);
        $type = $v['type'];
        $sort = [];
        if (!empty($v['_sort'])) {
            $order = $v['_sort'];
            if ($order) {
                foreach (explode(',', $order) as $o) {
                    $i = SORT_ASC;
                    if ('-' === $o[0]) {
                        $i = SORT_DESC;
                        $o = substr($o, 1);

                    }
                    $sort[$o] = $i;
                }
            }
        }
        $accounts = Account::listAccounts($type, $v['_refresh']);
        if ($accounts) {
            foreach ($sort as $name => $order) {
                usort($accounts, function ($a, $b) use ($name, $order) {
                    $an = $a->$name;
                    $bn = $b->$name;
                    $d = is_numeric($an) && is_numeric($bn) ? $bn - $an : strcmp($bn, $an);
                    if (SORT_DESC === $order) {
                        $d = -$d;
                    }
                    return $d > 0 ? 1 : -1;
                });
            }
            $r = ['result' => $accounts];
            if ((boolean)$v['_meta']) {
                $r['meta'] = Account::getMetadata(\count($accounts) > 0 ? $accounts[0] : null);
            }
            return $r;
        }
        return ['success' => false];
    }

    public function name(Request $request)
    {
        return Account::getByName($request->get('name'), $request->get('type'));
    }

    public function send(Request $request)
    {
    }

    public function store(Request $request)
    {
        $account = Account::query()
            ->where(['id' => $request->get('id')])
            ->first();
        if ($account instanceof Account) {
            $account->fill($request->post());
            $changes = $account->getDirty();
            if (\count($changes) > 0) {
                return [
                    'success' => $account->update(),
                    'id' => $account->id,
                    'changes' => $changes,
                ];
            }
            return response(['success' => false, 'changes' => []], 400);
        }
        return response(['success' => false], 404);
    }
}
