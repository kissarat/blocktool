<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Tochka\JsonRpcClient\Client;

const FILLABLE = ['wallet', 'type', 'name'];

class Account extends Model
{
    public const WEI = 10 ** 18;
    public const GAS = 21000;
    protected static $_fee;

    protected $fillable = FILLABLE;

    protected static function getAttributesInfo(): array
    {
        return [
            'name' => 'Username',
            'type' => 'Currency',
            'balance' => ['label' => 'Balance', 'type' => 'number', 'mono' => true],
            'wallet' => ['label' => 'Wallet', 'mono' => true]
        ];
    }

    public static function getMetadata($model)
    {
        $attributes = [];
        if (!$model) {
            $model = new static();
        }
        $attrs = static::getAttributesInfo();
        $attrs['id'] =  ['label' => 'ID', 'readonly' => true];
        if ($model->timestamps) {
            $attrs['created_at'] = ['label' => 'Created', 'type' => 'timestamp', 'mono' => true];
            $attrs['updated_at'] = ['label' => 'Updated', 'type' => 'timestamp', 'mono' => true];
        }
        foreach ($attrs as $name => $label) {
            $attribute = [];
            if (\is_array($label)) {
                foreach ($label as $k => $v) {
                    $attribute[$k] = $v;
                }
            } else {
                $attribute['label'] = $label;
            }
            if (empty($attribute['name'])) {
                $attribute['name'] = $name;
            }
            $attribute['readonly'] = !\in_array($name, FILLABLE, true);
            $attributes[] = $attribute;
        }
        return [
            'table' => $model->table,
            'primaryKey' => $model->primaryKey,
            'incrementing' => $model->incrementing,
            'attributes' => $attributes
        ];
    }

    public static function listAccounts(string $type, bool $refresh)
    {
        if ($refresh) {
            $eth = Client::get($type);
            $accounts = [];
            $r = $eth->eth_accounts();
            if ($r->success) {
                foreach ($r->data as $wallet) {
                    /**
                     * @var Account $account
                     */
                    $account = static::firstOrCreate([
                        'wallet' => $wallet,
                        'type' => $type
                    ]);
                    if ($account->updateBalance()) {
                        $account->save();
                        $accounts[] = $account;
                    }
                }
                return $accounts;
            }
            return null;
        }
        return static::query()
            ->where(['type' => $type])
            ->getModels();
    }

    public static function listByBalance($type, $min, $sort = SORT_ASC)
    {
        $src = static::listAccounts($type);
        if ($src instanceof Collection) {
            $accounts = $src->filter(function ($account) use ($min) {
                return $account->balance >= $min;
            });
            return SORT_ASC === $sort ? $accounts->sortBy('balance') : $accounts->sortByDesc('balance');
        }
        return null;
    }

    public function updateBalance()
    {
        $eth = Client::get($this->type);
        $b = $eth->eth_getBalance($this->wallet);
        if ($b->success) {
            $this->balance = round(hexdec($b->data) / static::WEI, 6);
        }
        return $b->success;
    }

    public static function getByName($type, $name)
    {
        $must = ['type' => $type, 'name' => $name];
        $account = static::query()
            ->where($must)
            ->first();
        if (!$account) {
            $eth = Client::get($type);
            $account = static::query()
                ->where([
                    'name' => null,
                    'type' => $type
                ])
                ->first();
            if ($account) {
                $account->name = $name;
            } else {
                $w = $eth->personal_newAccount();
                if ($w->successs) {
                    $account = new static($must);
                }
            }
            $eth->parity_setAccountName($account->wallet, $account->name);
        }
        $account->updateBalance();
        if ($account->save()) {
            return $account;
        }
        return null;
    }

    public static function getFee($type)
    {
        if (null === static::$_fee) {
            $eth = Client::get($type);
            $p = $eth->eth_gasPrice();
            if ($p->success) {
                static::$_fee = (hexdec($p->data) * static::GAS) / static::WEI;
            }
        }
        return static::$_fee;
    }

    public function send($to, $amount, $password = 'Ahshieseechami6yoojaebahng4eegaewool2Que')
    {
        $response = [];
        $fee = static::getFee($this->type);
        $amount -= $fee;
        if ($amount >= 0 && $amount <= $this->balance) {
            $eth = Client::get($this->type);
            $response['old'] = $this->balance;
            $t = $eth->personal_sendTransaction([
                'to' => $to,
                'from' => $this->wallet,
                'gas' => '0x' . dechex(static::GAS),
                'value' => '0x' . dechex($amount)
            ]);
            if ($t->success) {
                $this->updateBalance();
                $response['new'] = $this->balance;
                $response['amount'] = $amount;
                $response['fee'] = $fee;
                $response['txid'] = $t->data;
                return $response;
            }
        }
        return null;
    }

    public static function sendSingle($type, $target, $amount, &$accounts)
    {
        foreach ($accounts as $account) {
//            if ($account->balance >= )
        }
    }

    public static function sendMany($type, $targets)
    {
        $fee = static::getFee($type);
        $accounts = static::listByBalance($type, $fee);

    }
}
